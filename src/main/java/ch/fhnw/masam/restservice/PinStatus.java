package ch.fhnw.masam.restservice;

import java.io.Serializable;

public class PinStatus implements Serializable {

    private int pinId;
    private boolean state;

    public int getPinId() {
        return pinId;
    }

    public void setPinId(int pinId) {
        this.pinId = pinId;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}
