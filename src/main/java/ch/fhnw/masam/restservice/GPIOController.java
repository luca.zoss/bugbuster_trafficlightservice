package ch.fhnw.masam.restservice;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import jakarta.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("trafficlight")
public class GPIOController {

	private static final Logger LOG = LoggerFactory.getLogger(GPIOController.class);
	
	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@Value("${gpiomode}")
	private String gpioMode;

	@PostConstruct
	void init() {
		GPIOFactory.setGPIOMode(gpioMode);
	}

	@GetMapping("/greeting")
	public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
		return new Greeting(counter.incrementAndGet(), String.format(template, name));
	}

	@GetMapping("/status")
	public List<PinStatus> status(@RequestParam(value = "pinids") List<Integer> pinIds) {
		LOG.debug("status request: " + pinIds.toString());
		LOG.info("status request for BugBusters: " + pinIds.toString());
		try {
			List<PinStatus> result = new ArrayList<PinStatus>();

			if (pinIds == null) {
				return result;
			}
			for (Integer pinId : pinIds) {
				PinStatus pinStatus = new PinStatus();
				boolean state = GPIOFactory.getInstance().getState(pinId);
				pinStatus.setState(state);
				pinStatus.setPinId(pinId);
				result.add(pinStatus);
			}

			return result;
		} catch (Exception ex) {
			// TODO: log file entry
			LOG.error(ex.getMessage(), ex);
			throw new RuntimeException(ex);
		}
	}

	private List<Long []> someData = new ArrayList<>();

	private void increaseMemoryUsage() {
		someData.add(new Long[10000]);
	}

	@PutMapping("/pin")
	public boolean setPin(@RequestParam(value = "state") int status, @RequestParam(value = "pinid") int pinid) {
		LOG.info("incoming request: state=" + status + ", pinid=" + pinid);
		try {
			GPIOFactory.getInstance().setState(pinid, status);
		} catch (Exception ex) {
			LOG.error(ex.getMessage(), ex);
			throw new RuntimeException(ex);
		}
		return true;
	}
}
