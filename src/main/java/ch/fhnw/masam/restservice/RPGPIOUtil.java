package ch.fhnw.masam.restservice;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pi4j.Pi4J;
import com.pi4j.context.Context;
import com.pi4j.io.gpio.digital.DigitalOutput;
import com.pi4j.io.gpio.digital.DigitalOutputProvider;

public class RPGPIOUtil implements IGPIOUtil {

	private static final Logger LOG = LoggerFactory.getLogger(RPGPIOUtil.class);
	
	private Context ctx = null;
	private Map<Integer, DigitalOutput> pins = new HashMap<>();
	
	private void createContext() {
		LOG.info("create context...");
		ctx = Pi4J.newAutoContext();
	}
	
    public boolean getState(int pin) throws Exception {
    	if (ctx == null) createContext();


        DigitalOutput output = null;
        if (pins.containsKey(pin)) {
            output = pins.get(pin);
        } else {
            var config = DigitalOutput.newConfigBuilder(ctx)
                    .address(pin)
                    .build();
            DigitalOutputProvider digitalInputProvider = ctx.provider("pigpio-digital-output");
            output = digitalInputProvider.create(config);
            pins.put(pin, output);
        }
        boolean result = output.isHigh() ? true : false;

        return result;
    }

    public void setState(int pin, int state) throws Exception {

        // TODO: check pin id if valid
    	if (ctx == null) createContext();

        DigitalOutput output = null;
        if (pins.containsKey(pin)) {
            output = pins.get(pin);
        } else {
            var config = DigitalOutput.newConfigBuilder(ctx)
                    .address(pin)
                    .build();
            DigitalOutputProvider digitalInputProvider = ctx.provider("pigpio-digital-output");
            output = digitalInputProvider.create(config);
            pins.put(pin, output);
        }

        if (state == STATE_HIGH) {
            output.high();
        }

        else if (state == STATE_LOW) {
            output.low();
        }

        else {
            // invalid state
        	LOG.error("invalid state: " + state);
        }

        //pi4j.shutdown();
    }

}
